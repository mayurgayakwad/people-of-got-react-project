import "./App.css";
import data from "./data_2";
import { useState } from "react";
function Header() {
  return (
    <div className="header">
      <h1>People of GOT 👑</h1>
      <input placeholder="search the people of GOT"></input>
    </div>
  );
}

function ButtonAndPeoples() {
  let copyData = data.houses;
  const [state, setState] = useState(copyData);
  const FilterFun = (element) => {
    let UpadteData = copyData.filter((ele) => ele.name === element);
    setState(UpadteData);
  };
  return (
    <>
      <div className="fiterSection">
        {data.houses.map((ele) => (
          <button
            className={`searchButtons`}
            key={ele.name}
            onClick={() => FilterFun(ele.name)}
          >
            {ele.name}
          </button>
        ))}
      </div>
      <PeopleSection info={state} />
    </>
  );
}

function PeopleSection(probs) {
  return (
    <div className="peopleSection">
      {probs.info.map((ele) =>
        ele.people.map((x, index) => (
          <>
            <div className="peopleDiv" key={index}>
              <img
                className="images"
                src={x.image}
                key={x.image.toString()}
                alt={x.name}
              />
              <div className="names" key={x.name}>
                {x.name}
              </div>
              <div className="description" key={x.description}>
                {x.description}
              </div>
              <a href={x.wikiLink}>
                <button className="knowMoreBtn" key={x.wikiLink}>
                  KNOW MORE!
                </button>
              </a>
            </div>
          </>
        ))
      )}
    </div>
  );
}

export { Header, ButtonAndPeoples };
